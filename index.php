<!DOCTYPE html>
<html lang="en">
<head>
  <title>API TEST PHP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="jumbotron text-center">
  <h1>API TEST</h1>
  <p>PHP decode and encode json from API to mariaDB</p> 
</div>
  
<div class="container">
<a href="data.php" class="btn btn-success">Send Data</a>
  <a href="get.php" class="btn btn-success">Take Data</a>
  <p id="place"></p>
</div>

<script>
    $().ready(function(){
        $('#send').on('click',function(e){
          $.getJSON('http://localhost/jsonencode/data.php', function(data) {
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                // 'headers':{
                //   'Content-Type':'application/json'
                // },
                'url': 'http://103.56.206.202:3000/customers',
                'contentType': 'application/json; charset=utf-8',
                'dataType':'json',
                'data': JSON.stringify(data),
                'success': function(data1){
                    console.log(data1);
                }
            });
          });
        });
    });

    // $().ready(function(){
    //     $('#take').on('click',function(e){
    //         e.preventDefault();
    //         $.ajax({
    //             'type': 'GET',
    //             'url': 'http://103.56.206.202:3000/customers',
    //             'success': function(data1){
    //                 console.log(data1);
    //             }
    //         });
    //     });
    // });
</script>

</body>
</html>
