<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="jumbotron text-center">
  <h1>My First Bootstrap Page</h1>
  <p>Resize this responsive page to see the effect!</p> 
</div>
  
<div class="container">
  <button class="btn btn-success" id="send">Send Data</button>
  <button class="btn btn-success" id="take">Take</button>
  <p id="place"></p>
</div>
<?php
$server = "localhost";
$user = "root";
$password = "";
$nama_database = "sister";

$db = mysqli_connect($server, $user, $password, $nama_database);

if( !$db ){
    die("Gagal terhubung dengan database: " . mysqli_connect_error());
}

$return_arr = array();

$fetch = mysqli_query($db,"SELECT * FROM tbmutasitoko"); 
$i=0;
while ($row = mysqli_fetch_array($fetch, MYSQLI_ASSOC)) {
    $i++;
    $row_array['nomutasi'] = $row['nomutasi'];
    $row_array['tglmutasi'] = $row['tglmutasi'];
    $row_array['kddivisidari'] = $row['kddivisidari'];

    array_push($return_arr,$row_array);

    //$pages_array[] = (object) array('nomutasi' => $row['nomutasi']);
}

$dataa = json_encode(array('mutasi' => $return_arr), JSON_FORCE_OBJECT);

?>
<script>
    $().ready(function(){
        $('#send').on('click',function(e){
          $.getJSON('http://localhost/jsonencode/data.php', function(data) {
            e.preventDefault();
            $.ajax({
                'type': 'POST',
                // 'headers':{
                //   'Content-Type':'application/json'
                // },
                'url': 'http://103.56.206.202:3000/customers',
                'contentType': 'application/json; charset=utf-8',
                'dataType':'json',
                'data': JSON.stringify(data),
                'success': function(data1){
                    console.log(data1);
                }
            });
          });
        });
    });
    
    $().ready(function(){
        $('#take').on('click',function(e){
            e.preventDefault();
            $.ajax({
                'type': 'GET',
                'url': 'http://103.56.206.202:3000/customers',
                'success': function(data1){
                    console.log(data1);
                }
            });
        });
    });
</script>

</body>
</html>
